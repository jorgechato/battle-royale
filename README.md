# GO Battle Royal
[![Build Status](https://travis-ci.com/jorgechato/battle-royale.svg?branch=master)](https://travis-ci.com/jorgechato/battle-royale)
[![SonarCloud](https://sonarcloud.io/api/project_badges/measure?project=com.jorgechato.battle-royale&metric=coverage)](https://sonarcloud.io/dashboard?id=com.jorgechato.battle-royale)
[![SonarCloud](https://sonarcloud.io/api/project_badges/measure?project=com.jorgechato.battle-royale&metric=alert_status)](https://sonarcloud.io/dashboard?id=com.jorgechato.battle-royale)
[![Go Report Card](https://goreportcard.com/badge/github.com/jorgechato/battle-royale)](https://goreportcard.com/report/github.com/jorgechato/battle-royale)
[![Godoc](https://img.shields.io/badge/go-documentation-blue.svg)](https://godoc.org/github.com/jorgechato/battle-royale)
[![Docker Repository on Quay](https://quay.io/repository/orggue/battle/status "Docker Repository on Quay")](https://quay.io/repository/orggue/battle)

Battle royal between different languages in the backend side [GO vs JS vs Java vs Kotlin vs Vertx]

In these repository you will find a backend written in GO with [mux](https://github.com/gorilla/mux) as a powerful URL router and dispatcher.
In the following list you will find my colleagues' repos:

- [TS with nestJS](https://github.com/RecuencoJones/nestjs-perf-test) written by @RecuencoJones

## Performance test

## Metrics

